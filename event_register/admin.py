from django.contrib import admin
from .models import EventRegister, Member

# Register your models here.
admin.site.register(EventRegister)
admin.site.register(Member)