from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .forms import Event_Form_Member, Event_Form_Guest
from .models import EventRegister, Member
from event_list.models import Event
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
import json


# Create your tests here.

class EventRegisterUnitTest(TestCase):
    def test_event_register_url_is_exist_and_post(self):
        Event.objects.create(judul='Test', kategori='Test', tanggal='2000-2-2',
                             penyelenggara='Test', deskripsi='Test', gambar='Test.jpg')
        response = Client().get('/event-register/1/')
        self.assertEqual(response.status_code, 200)
        name = 'test'
        Member.objects.create(fullname=name, username=name, email='test@email.com', birthday_date='2000-1-1',
                              password='test')
        response = Client().post('/event-register/1/',
                                 {'username': 'wrong username', 'email': 'test@email.com', 'password': 'test',
                                  'button': 'Book'})
        self.assertEqual(response.status_code, 200)

        response = Client().post('/event-register/1/',
                                 {'username': 'test', 'email': 'test@email.com', 'password': 'test',
                                  'button': 'Book'})
        self.assertEqual(response.status_code, 302)

        response = Client().post('/event-register/1/',
                                 {'username': 'test', 'email': 'test@email.com', 'password': 'test',
                                  'button': 'Book'})
        self.assertEqual(response.status_code, 200)

        response = Client().post('/event-register/1/',
                                 {'username': 'test', 'email': 'emailsalah@email.com', 'password': 'test',
                                  'button': 'Book'})
        self.assertEqual(response.status_code, 200)

        response = Client().post('/event-register/1/',
                                 {'username': 'test', 'email': 'test@email.com', 'password': 'passwordsalah',
                                  'button': 'Book'})
        self.assertEqual(response.status_code, 200)

        response = Client().post('/event-register/1/',
                                 {'username': 'test', 'email': 'test@email.com', 'password': 'test',
                                  'button': 'Book'})
        self.assertEqual(response.status_code, 200)

        response = Client().post('/event-register/1/',
                                 {'name': 'test', 'email': 'test@email.com',
                                  'button': 'Book'})
        self.assertEqual(response.status_code, 200)

        response = Client().post('/event-register/1/',
                                 {'name': 'test', 'email': 'test1@email.com',
                                  'button': 'Book as Guest'})
        self.assertEqual(response.status_code, 302)

        response = Client().post('/event-register/1/',
                                 {'name': 'test', 'email': 'test1@email.com',
                                  'button': 'Book as Guest'})
        self.assertEqual(response.status_code, 200)

        response = Client().post('/event-register/1/',
                                 {'username': 'test', 'email': 'test@email.com', 'password': 'test',
                                  'button': 'Book as Guest'})
        self.assertEqual(response.status_code, 200)

    def test_event_register_using_index_func(self):
        Event.objects.create(judul='Test', kategori='Test', tanggal='2000-2-2',
                             penyelenggara='Test', deskripsi='Test', gambar='Test.jpg')
        found = resolve('/event-register/1/')
        self.assertEqual(found.func, index)

    def test_form_validation_for_non_blank_items(self):
        Event.objects.create(judul='Test', kategori='Test', tanggal='2000-2-2',
                             penyelenggara='Test', deskripsi='Test', gambar='Test.jpg')
        name = "Tester"
        form = Event_Form_Member(data={'username': 'test', 'email': 'test@mail.com',
                                       'password': 'jangankepo', 'event': Event.objects.get(pk=1).id})
        self.assertTrue(form.is_valid())

        form2 = Event_Form_Guest(data={'name': name, 'email': 'test@mail.com', 'event': Event.objects.get(pk=1).id})
        self.assertTrue(form2.is_valid())

    def test_form_validation_for_blank_items(self):
        form = Event_Form_Member(data={'username': '', 'email': '',
                                       'password': '', 'event': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.error_messages['required'],
            "Tolong isi input ini"
        )

        form2 = Event_Form_Guest(data={'name': '', 'email': '', 'event': ''})
        self.assertFalse(form2.is_valid())
        self.assertEqual(
            form2.error_messages['required'],
            "Tolong isi input ini"
        )

    def test_model_event_register_str(self):
        Event.objects.create(judul='Test', kategori='Test', tanggal='2000-2-2',
                             penyelenggara='Test', deskripsi='Test', gambar='Test.jpg')
        name = "Test"
        EventRegister.objects.create(name=name, email='test@email.com', event=Event.objects.get(pk=1))
        self.assertEqual(name, str(EventRegister.objects.get(pk=1)))

    def test_model_member_str(self):
        name = "Test"
        Member.objects.create(fullname=name, username=name, email='test@email.com', birthday_date='2000-1-1',
                              password='test')
        self.assertEqual(name, str(Member.objects.get(pk=1)))

    def test_username_exists(self):
        name = "Test"
        Member.objects.create(fullname=name, username=name, email='test@email.com', birthday_date='2000-1-1',
                              password='test')
        self.assertTrue(user_check(name))

    def test_username_and_email_exists(self):
        name = "Test"
        Member.objects.create(fullname=name, username=name, email='test@email.com', birthday_date='2000-1-1',
                              password='test')
        self.assertTrue(user_email_check(name, 'test@email.com'))

    def test_username_has_right_password(self):
        name = "Test"
        Member.objects.create(fullname=name, username=name, email='test@email.com', birthday_date='2000-1-1',
                              password='test')
        self.assertTrue(user_password_check(name, 'test'))

    def test_already_registered_to_an_event(self):
        Event.objects.create(judul='Test', kategori='Test', tanggal='2000-2-2',
                             penyelenggara='Test', deskripsi='Test', gambar='Test.jpg')
        EventRegister.objects.create(name='test', email='test@email.com', event=Event.objects.get(pk=1))
        self.assertTrue(registered('test@email.com', Event.objects.get(pk=1)))

    def test_post_logged_in(self):
        Event.objects.create(judul='Test', kategori='Test', tanggal='2000-2-2',
                             penyelenggara='Test', deskripsi='Test', gambar='Test.jpg')
        user = User.objects.create(username='testuser', email='test@email.com')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        self.assertTrue(logged_in)

        response = c.post('/event-register/daftar/1')
        self.assertTrue(json.loads(response.content)['success'])
        self.assertEqual(response.status_code, 200)

        response = c.post('/event-register/daftar/1')
        self.assertTrue(json.loads(response.content)['registered'] and json.loads(response.content)['fail'])

    def test_post_not_logged_in(self):
        Event.objects.create(judul='Test', kategori='Test', tanggal='2000-2-2',
                             penyelenggara='Test', deskripsi='Test', gambar='Test.jpg')

        c = Client()

        response = c.post('/event-register/daftar/1')
        self.assertTrue(json.loads(response.content)['fail'])
        self.assertEqual(response.status_code, 200)
