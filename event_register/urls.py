from django.urls import path
from . import views

urlpatterns = [
    path('<int:pk>/', views.index, name='index'),
    path('daftar/<int:pk>', views.daftar, name='daftar'),
]
