from django.apps import AppConfig


class EventRegisterConfig(AppConfig):
    name = 'event_register'
