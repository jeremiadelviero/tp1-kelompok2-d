from django import forms


class Event_Form_Member(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }
    username = forms.CharField(label='Username', required=True, max_length=300,
                               widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label='Email', required=True, max_length=300,
                             widget=forms.EmailInput(attrs=attrs))
    password = forms.CharField(label='Password', required=True, max_length=300,
                               widget=forms.PasswordInput(attrs=attrs))


class Event_Form_Guest(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }
    name = forms.CharField(label='Nama Lengkap', required=True, max_length=300,
                           widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label='Email', required=True, max_length=300,
                             widget=forms.EmailInput(attrs=attrs))
#
