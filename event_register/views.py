from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from .forms import Event_Form_Guest, Event_Form_Member
from .models import EventRegister, Member
from event_list.models import Event


# Create your views here.
def index(request, pk):
    response = {'form_member': Event_Form_Member, 'form_guest': Event_Form_Guest, 'id': pk,
                'event': Event.objects.filter(pk=pk)[0]}
    if request.method == "POST":
        if 'Book as Guest' == request.POST['button']:
            form = Event_Form_Guest(request.POST or None)
            if form.is_valid():
                name = request.POST['name']
                email = request.POST['email']
                event = Event.objects.filter(pk=pk)[0]
                if registered(email, event):
                    response['fail_guest'] = 'Email sudah terdaftar untuk event ini'
                    return render(request, 'event_register/index.html', response)
                else:
                    register = EventRegister(name=name, email=email, event=event)
                    register.save()
                    return HttpResponseRedirect('/event-list/')
            else:
                return render(request, 'event_register/index.html', response)
        elif 'Book' == request.POST['button']:
            form = Event_Form_Member(request.POST or None)
            if form.is_valid():
                username = request.POST['username']
                email = request.POST['email']
                password = request.POST['password']
                event = Event.objects.filter(pk=pk)[0]
                if user_check(username):
                    if user_email_check(username, email) \
                            and user_password_check(username, password):
                        if registered(email, event):
                            response['fail_member'] = 'Email sudah terdaftar untuk event ini'
                            return render(request, 'event_register/index.html', response)
                        else:
                            register = EventRegister(name=username, email=email,
                                                     event=event)
                            register.save()
                            return HttpResponseRedirect('/event-list/')
                    elif not user_email_check(username, email):
                        response['fail_member'] = 'Email Salah'
                        return render(request, 'event_register/index.html', response)
                    elif not user_password_check(username, password):
                        response['fail_member'] = 'Password Salah'
                        return render(request, 'event_register/index.html', response)
                else:
                    response['fail_member'] = 'Username ' + username + ' tidak ada'
                    return render(request, 'event_register/index.html', response)
            else:
                return render(request, 'event_register/index.html', response)
    else:
        return render(request, 'event_register/index.html', response)


def user_check(username):
    member = Member.objects.filter(username=username)
    if member:
        return True


def user_email_check(username, email):
    member = Member.objects.filter(username=username)
    if member and member[0].email == email:
        return True


def user_password_check(username, password):
    member = Member.objects.filter(username=username)
    if member and member[0].password == password:
        return True


def registered(email, event):
    register = EventRegister.objects.filter(email=email, event=event)
    if register:
        return True


def daftar(request, pk):
    event = Event.objects.filter(pk=pk)[0]
    if request.method == "POST" and request.user.is_authenticated:
        username = request.user.username
        email = request.user.email
        if registered(email, event):
            return JsonResponse({"registered": True, "fail": True})
        register = EventRegister(name=username, email=email, event=event)
        register.save()
        return JsonResponse({"success": True, "event": event.judul})
    return JsonResponse({"fail": True, "event": event.judul})
