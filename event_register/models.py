from django.db import models
from event_list.models import Event


class EventRegister(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Member(models.Model):
    fullname = models.CharField(max_length=300)
    username = models.CharField(max_length=300, unique=True)
    email = models.EmailField(unique=True)
    birthday_date = models.CharField(max_length=300)
    password = models.CharField(max_length=300)
    address = models.CharField(max_length=300, null=True)

    def __str__(self):
        return self.fullname
