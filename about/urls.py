from django.urls import path
from . import views

urlpatterns = [
    path('', views.about, name='about'),
    path('tambah_komentar/', views.tambah_komentar, name='tambah_komentar'),
]
