from django.shortcuts import render
from .models import Event
from event_register.models import EventRegister


# Create your views here.
def eventList(request):
    event = Event.objects.all()
    event_register = EventRegister.objects.all()
    return render(request, 'event_list/event.html', {'Event': event, 'event_register': event_register})

def eventListed(request):
    event = Event.objects.all()
    event_register = EventRegister.objects.all()
    return render(request, 'event_list/evented.html', {'Event': event, 'event_register': event_register})