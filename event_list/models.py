from django.db import models


# Create your models here.
class Event(models.Model):
    judul = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
    tanggal = models.CharField(max_length=50)
    penyelenggara = models.CharField(max_length=50)
    deskripsi = models.TextField()
    gambar = models.TextField()

    def __str__(self):
        return self.judul
