from django.urls import path
from . import views


urlpatterns = [
    path('', views.eventList, name='eventList'),
    path('listed/', views.eventListed, name='eventListed'),
]
