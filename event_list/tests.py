from django.test import TestCase
from django.test import Client

# Create your tests here.
class EventListUnitTest(TestCase):
	def test_event_list_url_is_exist(self):
		response = Client().get('/event-list/')
		self.assertEqual(response.status_code, 200)

	def test_event_listed_url_is_exist(self):
		response = Client().get('/event-list/listed/')
		self.assertEqual(response.status_code, 200)

	def test_urls_using_event_template(self):
		response = Client().get('/event-list/')
		self.assertTemplateUsed(response, 'event_list/event.html')

	def test_urls_using_evented_template(self):
		response = Client().get('/event-list/listed/')
		self.assertTemplateUsed(response, 'event_list/evented.html')

