from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import BeritaTerbaru


# Create your tests here.
class newsTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_details(self):
        BeritaTerbarus = BeritaTerbaru.objects.create(title="title", gambar="test", judul="test", deskripsi="test")     
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

# Create your tests here.
class newsModelTest(TestCase):
    def test_if_instance_is_created(self):
        BeritaTerbaru.objects.create(title="title", gambar="test", judul="test", deskripsi="test")
        hitung_count = BeritaTerbaru.objects.count()
        self.assertEqual(hitung_count, 1)