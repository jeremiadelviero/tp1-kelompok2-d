from django.db import models


# Create your models here.
class BeritaTerbaru(models.Model):
    title = models.CharField(max_length=200)
    gambar = models.TextField()
    judul = models.TextField()
    deskripsi = models.TextField()

    def __str__(self):
        return self.title
