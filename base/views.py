from django.shortcuts import render
from .models import BeritaTerbaru

# Create your views here.
def index(request):
	berita = BeritaTerbaru.objects.all()
	return render(request, 'base/index.html', {'BeritaTerbaru' : berita}, {'title': 'Home'})
