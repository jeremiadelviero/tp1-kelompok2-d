from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import Testimoni
# Create your tests here.

class aboutVTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code,200)

	def test_using_update_news_template(self):
		response = Client().get('/about/')
		self.assertTemplateUsed(response,'testimoni/testimoni.html')

	def test_url_using_about_page(self):
		found = resolve('/about/')
		self.assertEqual(found.func, about)

	def test_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/about/tambah_komentar/', {'user_name':'test', 'komentar': 'test'})
		self.assertEqual(response_post.status_code, 200)
		
	def test_add_object_and_render(self):
		user_name = "Test"
		komentar = "Test"
		komentar = Testimoni.objects.create(user_name=user_name, komentar=komentar)
		response = Client().get("/about/")
		html_response = response.content.decode('utf8')
		self.assertIn(user_name, html_response)
		#self.assertIn(komentar, html_response)

class aboutModelTest(TestCase):
	def test_if_instance_is_created(self):
		Testimoni.objects.create(user_name="title", komentar="title")
		hitung_count = Testimoni.objects.count()
		self.assertEqual(hitung_count, 1)
