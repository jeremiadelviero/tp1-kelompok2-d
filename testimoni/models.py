from django.db import models

# Create your models here.

class Testimoni(models.Model):
	user_name = models.CharField(max_length=70)
	komentar = models.TextField()
	created_date = models.DateTimeField(auto_now=True, auto_now_add=False)