$(document).ready(function () {
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    function daftar(id) {
        $.ajax({
                type: "POST",
                url: "/event-register/daftar/" + id,
                data: {},
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        alert("Daftar berhasil");
                    } else if (response.fail) {
                        if (response.registered) {

                        } else {
                            alert("Daftar gagal");
                        }
                    }
                }
            });
    }
    //
    // function openModal() {
    //
    // }
});