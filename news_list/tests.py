from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, item
from .models import News


# Create your tests here.
class newsTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)

    def test_url_using_index_func(self):
    	found = resolve('/news/')
    	self.assertEqual(found.func, index)

    def test_details(self):
        newss = News.objects.create(title="title", pic="test", url="test")
        link = '/news/item/' + str(newss.pk)+'/'      
        response = Client().get(link)
        self.assertEqual(response.status_code, 200)

    def test_item_offset_error_redirect(self):
        newss = News.objects.create(title="title", pic="test", url="test")
        response = Client().get('/news/item/7/')
        self.assertEqual(response.status_code, 302)

# Create your tests here.
class newsModelTest(TestCase):
    def test_if_instance_is_created(self):
        News.objects.create(title="title",
                            pic="test",
                            url="test")
        hitung_count = News.objects.count()
        self.assertEqual(hitung_count, 1)