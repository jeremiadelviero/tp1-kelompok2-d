from django.shortcuts import render, HttpResponse
from django.http import Http404, HttpResponseRedirect
from .models import News

# Create your views here.
def index(request):
    news = News.objects.all()
    return render(request, 'news_list/news.html', {"news" : news})

def item(request, offset):
    try:
        news_select = News.objects.get(pk=offset)
        response = {}
        response['title'] = news_select.title
        response['details'] = news_select.url
        response['photo'] = news_select.pic
        html = 'news_list/news_item.html'
        return render(request, html, response)
    except:
        return HttpResponseRedirect('/news')
    
