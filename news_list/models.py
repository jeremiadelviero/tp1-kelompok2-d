from django.db import models

# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=200)
    pic = models.TextField(max_length=900, default='')
    url = models.TextField()