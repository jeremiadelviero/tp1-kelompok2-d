from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from . import views

app_name = 'news'
urlpatterns = [
    url(r'^$', views.index, name = 'index'),
    url(r'item/(\d+)/$', views.item, name = 'item'),
]
