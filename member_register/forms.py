from django import forms


class RegisterForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    attrsdate = {
        'type': 'date'
    }

    name = forms.CharField(label="Nama Lengkap", max_length=30, widget=forms.TextInput(attrs=attrs))
    username = forms.CharField(label="Username", max_length=12)
    email = forms.EmailField(label="Email", max_length=254)
    birth_date = forms.CharField(label="Tanggal Lahir",
                                 widget=forms.widgets.DateInput(format="%d/%m/%Y", attrs=attrsdate))
    password = forms.CharField(
        label="Kata Sandi",
        widget=forms.PasswordInput()
    )
    alamat = forms.CharField(
        label="Alamat Rumah",
        max_length=100,
        widget=forms.TextInput(),
        # help_text='Write here your message!'
    )

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        name = cleaned_data.get('name')
        username = cleaned_data.get('username')
        email = cleaned_data.get('email')
        birth_date = cleaned_data.get('birth_date')
        alamat = cleaned_data.get('alamat')
        if not name and not email and not alamat and not username and not birth_date:
            raise forms.ValidationError('You have to write something!')
