from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import RegisterForm
from event_register.models import Member


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            name = request.POST['name']
            username = request.POST['username']
            email = request.POST['email']
            birth_date = request.POST['birth_date']
            password = request.POST['password']
            alamat = request.POST['alamat']
            member_register = Member(fullname=name, username=username, email=email, birthday_date=birth_date,
                                     password=password, address=alamat)
            member_register.save()
            return HttpResponseRedirect('/event-list/')
    else:
        form = RegisterForm()
    return render(request, 'member_register/register.html', {'form': form})
# Create your views here.
