from django.apps import AppConfig


class MemberRegisterConfig(AppConfig):
    name = 'member_register'
