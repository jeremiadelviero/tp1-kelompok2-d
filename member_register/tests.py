from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from .views import register
from .forms import RegisterForm


class Story6Test(TestCase):
    def test_tugas_pemrograman_1_url_is_exist(self):
        response= Client().get('/daftar/')
        self.assertEqual(response.status_code, 200)

    def test_tugas_pemrograman_1_using_register_func(self):
        found = resolve('/daftar/')
        self.assertEqual(found.func, register)

    def test_form_validation_for_blank_name(self):
        form = RegisterForm(data={'name': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )

    def test_form_validation_for_blank_username(self):
        form = RegisterForm(data={'username': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'],
            ["This field is required."]
        )

    def test_form_validation_for_blank_email(self):
        form = RegisterForm(data={'email': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['email'],
            ["This field is required."]
        )

    def test_form_validation_for_blank_password(self):
        form = RegisterForm(data={'password': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'],
            ["This field is required."]
        )

    def test_form_validation_for_blank_address(self):
        form = RegisterForm(data={'alamat': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['alamat'],
            ["This field is required."]
        )

    def test_head_text_exist(self):
        request = HttpRequest()
        response = register(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Daftarkan Akun Sebagai Member', html_response)

    def test_agreement_exist(self):
        request = HttpRequest()
        response = register(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Dengan mendaftar', html_response)


