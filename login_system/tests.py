from django.test import TestCase, Client
from django.contrib.auth.models import User

class loginSystemTest(TestCase):
        def test_login_is_authenticated(self):
            user = User.objects.create(username='testuser')
            user.set_password('12345')
            user.save()

            c = Client()

            login = c.login(username='testuser', password='12345')

            self.assertTrue(login)

            response = c.get('/')
            html_response = response.content.decode('utf8')
            self.assertIn('Logout', html_response)


