from django.contrib.auth import logout as logout_user
from django.http import HttpResponseRedirect

def logout(request):
    logout_user(request)
    return HttpResponseRedirect('/')

# Create your views here.
